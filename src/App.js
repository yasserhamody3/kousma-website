import './App.css';
import Home from './Componenets/Home';
import { BrowserRouter as Router, Routes, Route, Form } from 'react-router-dom';
import Products from './Componenets/Product';
import Inbox from './Componenets/Inbox';
import {  productID } from './Componenets/Context/context';
import { useState } from 'react';
import Details from './Componenets/Details';

function App() {
  const [id, setID] = useState('');


  return (
      <productID.Provider value={{id , setID}}>
        <Router>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/products' element={<Products />} />
            <Route path='/products/details' element={<Details />} />
            <Route path='/inbox' element={<Inbox />} />
          </Routes>
        </Router>
      </productID.Provider>
  );
}

export default App;
