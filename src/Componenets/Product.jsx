import Navbar from "./Navbar";
import { useLocation } from "react-router-dom";
import label from '../Images/Home/label.jpg'
import { useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { motion } from "framer-motion";
import api from "./API/api";
import ReactLoading from "react-loading";

import { useRef } from "react";
import { productID } from "./Context/context";
import { click } from "@testing-library/user-event/dist/click";


function Products() {

    const navigate = useNavigate();
    const { id, setID } = useContext(productID);
    const [cick, setclick] = useState(false);
    const [loading, setloading] = useState(true);
    const products = useRef([{}])
    const products1 = useRef([{}]);
    const Categories1 = useRef([{}]);
    const Categories = useRef([{}]);


    const buttonVariants = {
        hover: {
            scale: 1.1,
            borderColor: "#fff",
            color: "#fff",
            transition: {
                duration: 0.2,
                ease: "easeInOut",
            },
        },
    };

    const ViewDetails = (event) => {
        const id = event.target.id;

        console.log(id);

        localStorage.setItem('id', id);
        navigate('/products/details')
    }



    useEffect(() => {

        api.get('/product').then((res) => {

            products1.current.unshift(res.data.data);

            setTimeout(() => {
                setloading(false)
            }, 2000)

            setTimeout(() => {
                products.current = products1.current[0]

            }, 1000)

        }).catch(error => {
            console.log(error);
        })

    }, [products]);

    useEffect(() => {
        setloading(true);
        api.get('/category/main').then((res) => {

            Categories1.current.unshift(res.data.data);
            setTimeout(() => {
                setloading(false)
            }, 2000)

            setTimeout(() => {
                Categories.current = Categories1.current[0]

            }, 1000)

        }).catch(error => {
            console.log(error);
        })

    }, []);

    const Filter_products = (event) => {
        const id = event.target.id;
        setID(id);
        setloading(true)
        setclick(true);
        api.get(`/product/by/category/${id}`).then((res) => {

            products1.current.unshift(res.data.data);

            setTimeout(() => {
                setloading(false)
            }, 2000)

            setTimeout(() => {
                products.current = products1.current[0]

            }, 1000)

        }).catch(error => {
            console.log(error);
        })

    };

    console.log(products.current)


    return (
        <div className="product h-screen">
            <nav className={`fixed w-full z-10`}>
                <Navbar />
            </nav>            {
                !loading && <div className="mt-10">

                    <div className="  flex justify-center items-center py-4 mt-40 sm:mt-6 md:mt-32">
                        {Categories.current.length > 0 &&
                            Categories.current.map((cat) => {
                                return (
                                    <motion.button
                                        className="mx-2 px-2 py-2 bg-transparent border border-white text-white rounded-lg"
                                        variants={buttonVariants}
                                        whileHover="hover"
                                        key={cat.id}
                                        id={cat.id}
                                        onClick={Filter_products}
                                    >
                                        {cat.name}
                                    </motion.button>
                                );
                            })}
                    </div>



                    {
                        !loading && click && <div className=" grid lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-2 gap-4 items-center ">

                            {

                                products.current.length > 0 && products.current.map((product) => {
                                    // console.log(product.Images[0].image);
                                    return (
                                        <motion.div
                                            key={product.id}
                                            className=" Card w-60 mx-auto rounded-lg shadow-lg"
                                            whileHover={{ scale: 1.1 }}
                                        >
                                            <img
                                                src={`https://timeengcom.com/cosma/${product.Images[0].image}`}
                                                className="w-full h-48"
                                                alt={product.name}
                                                loading="lazy"
                                            />
                                            <div className="px-6 py-4 flex flex-col justify-center items-center">
                                                <div className="mb-2">
                                                    <h4 className="text-xl font-semibold tracking-tight text-gray-800">
                                                        {product.name}
                                                    </h4>
                                                </div>
                                                <div className="flex justify-center items-center mb-2">
                                                    <div className="flex items-center">
                                                        <del className="text-gray-500">{product.discount_price}</del>
                                                        <span className="font-semibold text-green-600 pl-2">
                                                            {product.price}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <span className="font-medium text-rose-800">
                                                        {' '}
                                                        Sale :{product.discount}%
                                                    </span>
                                                </div>
                                                <div className="float-right mt-2">
                                                    <button
                                                        onClick={ViewDetails}
                                                        id={product.id}
                                                        className="bg-transparent hover:text-gray-800 text-slate-600 font-mono py-2 px-2"
                                                    >
                                                        View more
                                                    </button>
                                                </div>
                                            </div>
                                        </motion.div>

                                    );
                                })
                            }
                        </div>
                    }


                </div>
            }

            {
                loading && <div style={{ display: "flex", justifyContent: "center", alignItems: "center", marginTop: '40px' }}>
                    <ReactLoading type={"balls"} color={"gray"} height={50} width={50} />
                </div>
            }
        </div>
    )




}
export default Products;