import axios from 'axios';

const api = axios.create({
    baseURL: 'https://timeengcom.com/cosma',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
})
export default api;