const Label = () => {
  const variants = {
    hidden: {
      x: '-100%',
      opacity: 0,
    },
    visible: {
      x: 0,
      opacity: 1,
      transition: {
        duration: 0.5,
        ease: 'easeOut',
      },
    },
  };

  return (
    <div >
      <div id="Label" className="h-full bg-opacity-50 absolute inset-0">
      </div>
      <div className="container mx-auto h-full flex flex-col justify-center items-center">
        <h2 className="text-2xl font-bold text-gray-800 text-center mb-8">
          Section 2
        </h2>
        <div className="bg-white bg-opacity-75 max-w-md p-8 rounded-lg shadow-xl md:p-4">
          <h3 className="text-lg font-bold mb-4">Title</h3>
          <p className="text-gray-700">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, justo vitae ultricies condimentum, ipsum velit malesuada sem, non ullamcorper nisi nisl et nisi. Cras auctor turpis ac metus pharetra, vel pharetra turpis placerat. Mauris ac orci vel tellus accumsan placerat. Nullam vitae ultrices ex. Maecenas fringilla magna vel ex rhoncus, non varius purus euismod. Integer urna lorem, dignissim in sapien vel, lacinia pellentesque elit. Proin euismod vel turpis vel sollicitudin. </p>
        </div>
      </div>
    </div>
  );


}
export default Label;