import { motion } from "framer-motion";
import { useForm } from "react-hook-form";
import api from "./API/api";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

function Inbox() {

    
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navigate = useNavigate();

    const onSubmit = (data) => {
        console.log(data);

        api.post('/message', data).then((res) => {
            toast.success("Your information submitted successfully!");
            setTimeout(() => {
                navigate('/')
            }, 5000)

        }).catch((error) => {
            toast.error("Something went wrong");
            setTimeout(() => {
                window.location.reload();
            }, 5000)
            console.log(error);

        })
    };



    return (
        <div className=" inbox bg-transparent h-screen flex justify-center items-center">
            <ToastContainer  />
            <motion.div
                initial={{ opacity: 0, y: -50 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.5 }}
                className=" p-8 rounded-lg shadow-md"
            >
                <h2 className="text-2xl font-bold mb-4  text-slate-800">Contact Us</h2>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
                        <div>
                            <label htmlFor="firstName" className="block text-gray-700 font-bold mb-2"> Name</label>
                            <input
                                type="text"
                                id="contact_name"
                                {...register("contact_name", { required: "Contact Name is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.firstName ? "border-red-500" : ""}`}
                            />
                            {errors.contact_name && <p className="text-red-500 text-xs italic">{errors.contact_name.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="subject" className="block text-gray-700 font-bold mb-2">Your Subject</label>
                            <input
                                type="text"
                                id="subject"
                                {...register("subject", { required: "subject is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.lastName ? "border-red-500" : ""}`}
                            />
                            {errors.subject && <p className="text-red-500 text-xs italic">{errors.subject.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="email" className="block text-gray-700 font-bold mb-2">Email</label>
                            <input
                                type="email"
                                id="email"
                                {...register("email", { required: "Email is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.email ? "border-red-500" : ""}`}
                            />
                            {errors.email && <p className="text-red-500 text-xs italic">{errors.email.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="phone" className="block text-gray-700 font-bold mb-2">Phone Number</label>
                            <input
                                id="contact_number"
                                {...register("contact_number", { required: "contact Number is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.phone ? "border-red-500" : ""}`}
                            />
                            {errors.contact_number && <p className="text-red-500 text-xs italic">{errors.contact_number.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="companyName" className="block text-gray-700 font-bold mb-2">Company Name</label>
                            <input
                                type="text"
                                id="company_name"
                                {...register("company_name", { required: "Company Name is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.companyName ? "border-red-500" : ""}`}
                            />
                            {errors.company_name && <p className="text-red-500 text-xs italic">{errors.company_name.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="businessSector" className="block text-gray-700 font-bold mb-2">Business Sector</label>
                            <input
                                type="text"
                                id="business_sector"
                                {...register("business_sector", { required: "Business Sector is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.businessSector ? "border-red-500" : ""}`}
                            />
                            {errors.business_sector && <p className="text-red-500 text-xs italic">{errors.business_sector.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="country" className="block text-gray-700 font-bold mb-2">Country</label>
                            <input
                                type="text"
                                id="country"
                                {...register("country", { required: "Country is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.country ? "border-red-500" : ""}`}
                            />
                            {errors.country && <p className="text-red-500 text-xs italic">{errors.country.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="city" className="block text-gray-700 font-bold mb-2">City</label>
                            <input
                                type="text"
                                id="city"
                                {...register("city", { required: "City is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.city ? "border-red-500" : ""}`}
                            />
                            {errors.city && <p className="text-red-500 text-xs italic">{errors.city.message}</p>}
                        </div>
                        <div>
                            <label htmlFor="whatsappNumber" className="block text-gray-700 font-bold mb-2">WhatsApp Number</label>
                            <input
                                id="whatsapp_number"
                                {...register("whatsapp_number", { required: "WhatsApp Number is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.whatsappNumber ? "border-red-500" : ""}`}
                            />
                            {errors.whatsapp_number && <p className="text-red-500 text-xs italic">{errors.whatsapp_number.message}</p>}
                        </div>
                        <div className="col-span-2">
                            <label htmlFor="message" className="block text-gray-700 font-bold mb-2">Message</label>
                            <textarea
                                id="message"
                                {...register("message", { required: "Message is required" })}
                                className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.message ? "border-red-500" : ""}`}
                            ></textarea>
                            {errors.message && <p className="text-red-500 text-xs italic">{errors.message.message}</p>}
                        </div>
                    </div>
                    <div className="mt-6">
                        <button type="submit" className="bg-slate-800 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                            Submit
                        </button>
                    </div>
                </form>
            </motion.div>
        </div>
    );
}
export default Inbox 