import React from 'react';
import { Typewriter } from 'react-simple-typewriter';

function AboutUs() {

    return (
        <div className='h-screen'>
            <div className="h-screen flex items-center w-full ">
                
                <div className="hidden md:block lg:w-1/2 flex items-center justify-center">
                    <h1 className="text-9xl font-extrabold font- text-gray-400">
                        F&M <span className="text-purple-500">Cosma</span>
                    </h1>
                </div>
                <div className="font-bold text-slate-300 text-lg mx-7">
                    <p >
                        <h1 className=" md:w-full text-3xl font-extrabold font- text-slate-600">
                            F&M
                        </h1>
                        <Typewriter
                            words={[
                                ' industrial, founded in 2022, is a manufacturing company located in Umm Alquwain, UAE. The primary activity of F&M Industrial is to focus on producing and promoting products such as personal care, hair care, natural oils, perfumes, cosmetics, and home care products. F&M Industrial has its own brands including Bloom Beauty, Sunsation, YA, Challenger, and Super She...',
                            ]}
                            loop={1}
                            typeSpeed={30}
                        />
                    </p>
                </div>
            </div>
        </div>
    );
}

export default AboutUs;