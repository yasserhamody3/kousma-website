import React, { useCallback, useContext, useEffect } from "react";
import { motion } from "framer-motion";
import '../Componenets/style/Home.css';
import { useRef } from "react";
import { useState } from "react";
import ReactLoading from "react-loading";
import api from "./API/api";

function Details() {

    const containerVariants = {
        hidden: {
            opacity: 0,
            scale: 0.8,
        },
        visible: {
            opacity: 1,
            scale: 1,
            transition: {
                duration: 0.5,
            },
        },
    };

    const imageVariants = {
        hover: {
            scale: 1.7,
            transition: {
                duration: 1.1,
            },
        },
    };

    const [loading, setLoading] = useState(true);
    const product = useRef({});
    const Images = useRef([]);

    const [id, setId] = useState(() => {
        const storedID = localStorage.getItem('id');
        return storedID ? storedID : '';
    });


    useEffect(() => {
        localStorage.setItem('id', id);

        api.get(`/product/by/${id}`).then((res) => {

            product.current = res.data.data
            Images.current = res.data.data.Images.slice(1);
            if (res.status == 200) {
                setTimeout(() => {
                    setLoading(false);
                    console.log(product.current);
                    console.log(Images.current);

                }, 2000);
            }
        }).catch((error) => { console.log(error.message); })

    }, [id]);



    const OpenImage = (event) => {
        const src = event.currentTarget.getAttribute('src');
        window.open(src, '_blank');
    };

    return (
        <div className="details">
            {
                loading ? (
                    <div>
                        <div style={{ display: "flex", justifyContent: "center", alignItems: "center", marginTop: '40px' }}>
                            <ReactLoading type={"bubbles"} color={"white"} height={50} width={50} />
                        </div>
                    </div >
                ) : (
                    <div>
                        <motion.div
                            className=" details container mx-auto py-8 h-screen"
                            variants={containerVariants}
                            initial="hidden"
                            animate="visible"
                        >

                            <div className="md:flex md:items-center">
                                <motion.div
                                    className="w-full h-72 md:w-1/2 lg:h-screen"
                                    variants={imageVariants}
                                >
                                    <img
                                        onClick={OpenImage}
                                        className="h-full w-full rounded-md object-cover max-w-lg mx-auto"
                                        src={`https://timeengcom.com/cosma/${product.current.Images[0].image}`}
                                        alt="Product"
                                    />
                                </motion.div>
                                <div className="w-full max-w-lg mx-auto mt-3 md:ml-8 md:mt-0 md:w-1/2 ">
                                    <h1 className=" font-extrabold text-4xl font-mono text-gray-50 items-center justify-center">{product.current.name}</h1>
                                    <div className="flex items-center mt-1 justify-between">
                                        <span className=" font-bold text-xl  text-green-400">  {product.current.discount_price}</span>
                                        <span className="ml-2 text-sm text-gray-700 line-through font-medium"> {product.current.price}</span>
                                        <span className="ml-2 text-sm text-red-800">{product.current.discount_price}%  offer</span>
                                        <div className="justify-end mt-6">
                                            <h3 className="text-cyan-400 text-xl font-bold mt-5"> <span className="text-neutral-400 font-thin">{product.current.category}</span></h3>
                                            <h3 className="text-gray-700 text-xl font-bold mt-5">Size : <span className="text-stone-400">{product.current.Description.size}</span></h3>
                                        </div>
                                    </div>
                                    <hr className="text-slate-500 mt-9" />
                                    <p className="text-slate-300 mt-4  font-semibold">
                                        {product.current.Description.about}
                                    </p>
                                    <hr className="mt-5" />
                                    <div className="left-36">
                                        <p className="text-slate-300 mt-4 font-semibold left-36">
                                            {product.current.Description.how_to_use}
                                        </p>


                                        <p className="text-slate-300 mt-4 font-semibold">  </p>
                                        <div className="mt-6">
                                            {
                                                product.current.Images && <div className=" left-36 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 gap-4 mt-4">
                                                    {
                                                        Images.current.map((p) => (
                                                            <motion.img
                                                                className="h-32 w-40 rounded-md object-cover"
                                                                src={`https://timeengcom.com/cosma/${p.image}`}
                                                                alt="Product"
                                                                whileHover={{ scale: 1.1, borderRadius: "50%" }}
                                                                onClick={OpenImage}
                                                            />
                                                        ))
                                                    }
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </motion.div>
                    </div>
                )
            }
        </div >
    );
}

export default Details;