import { useState, useEffect, useRef } from 'react';
import AboutUs from './About';
import '../Componenets/style/Home.css';
import { FaFacebook, FaInstagram } from 'react-icons/fa';
import { motion } from 'framer-motion';
import Navbar from './Navbar';
import Development from './Development';
import { useNavigate } from 'react-router-dom';
import LoadingBar from 'react-top-loading-bar';


const LandingPage = () => {
  const navigate = useNavigate();

  const [scrollDirection, setScrollDirection] = useState('none');
  const prevScrollY = useRef(0);
  const [navBackground, setNavBackground] = useState("bg-transparent");
  const [finish, setFinish] = useState(false);
  const loadingBarRef = useRef(null);


  useEffect(() => {
    loadingBarRef.current.continuousStart();
    setTimeout(() => {
      loadingBarRef.current.complete();
    }, 500);

    const handleScroll = () => {
      const currentScrollY = window.scrollY;

      if (currentScrollY > prevScrollY.current) {
        setScrollDirection("up");
        setNavBackground("bg-gray-900");
      } else if (currentScrollY < prevScrollY.current) {
        setScrollDirection("down");
        setNavBackground("bg-transparent");
      }

      prevScrollY.current = currentScrollY;
    };

    window.addEventListener("scroll", handleScroll, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleLoaderFinished = () => {
    setFinish(true);
  }
  return (
    <div
      className="h-screen bg-cover bg-center"

    >
      <LoadingBar ref={loadingBarRef} onLoaderFinished={handleLoaderFinished} />

      {finish && <div className="relative z-0 flex flex-col h-screen ">
        <nav className={`fixed w-full z-10 ${navBackground}`}>
          <Navbar />
        </nav>


        <div className=" flex-grow">

          <div
            id="section1"
            className="bg-white h-screen bg-cover bg-center"
          >
            <div className='bg'>

              <div className="container  mb:mx-10" >
                <AboutUs />

              </div>
            </div>
          </div>

          <div className='Quality'>
            <div id="Quality" className=" h-screen">
              <div className="container mx-auto">
                <div className="container mx-auto">
                  <motion.h2
                    className="text-5xl font-bold text-sky-800 inline-block text-left mt-8 mr-4"
                    whileHover={{ letterSpacing: '0.2em' }}
                  >
                    Quality
                    Standards               </motion.h2>
                </div>
                <motion.div
                  className="flex items-center justify-center h-screen w-full"
                  style={{ backgroundColor: "transparent" }}
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                >
                  <div className="p-6 shadow-lg rounded-lg w-96">
                    <motion.p
                      className="text-gray-800 text-lg font-medium "
                      whileHover={{ scale: 1.1 }}
                      whileTap={{ scale: 0.9 }}
                      whileDrag={{ scale: 0.8 }}
                    >
                      F&M Industrial which is certified by GMP, All our
                      products are authorized to be placed on the market for
                      use inside the United Arab Emirates and outside, and
                      so match with the requirement of the UAE, Standards
                      Organization and is subject to UAE.
                    </motion.p>
                  </div>
                </motion.div>

              </div>
            </div>
          </div>



          <div className="dev">
            <div id="development" className="dev bg-gradient-to-br from-slate-700
           to-yellow-100 h-screen" style={{ backgroundImage: "url('../../Images/Home/dev.jpg')" }}
            >
              <div className="container mx-auto">
                <motion.h2
                  className="text-5xl font-bold text-gray-950 inline-block text-left mt-8 mr-4"
                  whileHover={{ letterSpacing: '0.2em' }}
                >
                  The Process
                </motion.h2>

                <Development />
              </div>
            </div>
          </div>

          <div className='label relative h-screen'>
            <div id="Label" className="h-full bg-opacity-50 absolute inset-0">
              {/* Transparent overlay */}
            </div>
            <div className="container mx-auto h-full flex flex-col justify-center items-center">
              <h2 className="text-2xl  text-slate-900 text-center mb-8 font-extrabold">
                Private Label Service
              </h2>
              <div className="bg-white bg-opacity-75 max-w-md p-8 rounded-lg shadow-xl md:p-4 p-6">
                <p className="text-gray-700">We are a trusted provider of comprehensive Private Label support services, leveraging
                  state-of-the-art production facilities and deep expertise. Our commitment is to collaborate
                  closely with our partners, oering invaluable market insight and sharing our experience to
                  ensure optimal product success. From initial ideation to product launching and beyond, we
                  oer a seamless journey, encompassing research and development, meticulous product
                  design, strategic packaging solutions, and vigilant monitoring of product maturity.
                  Whether you're at the idea stage or any point in between, our dedicated team is here to
                  guide you, turning your vision into a market-ready reality. </p>
              </div>
              <button onClick={() => {
                console.log('hay');
                navigate('/inbox')
              }} className=" z-10 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-4">
                Inbox us
              </button>
            </div>
          </div>
        </div>



        <div className="fixed bottom-0 left-0 z-10">
          <ul className="flex flex-col items-center">
            <li className="w-10 h-10 flex items-center justify-center hover:ml-[-10px] duration-700 bg-[#162DA4] mb-2">
              <a href="https://www.instagram.com/cosma.fandom" target="_blank" rel="noopener noreferrer">
                <FaInstagram className="h-6 w-6" />
              </a>
            </li>
            <li className="w-10 h-10 flex items-center justify-center hover:ml-[-10px] duration-700 bg-teal-800 mb-2">
              <a href="https://www.facebook.com/cosma.fandom" target="_blank" rel="noopener noreferrer">
                <FaFacebook className="h-6 w-6" />
              </a>
            </li>
          </ul>
        </div>

      </div>}
    </div>
  );
};

export default LandingPage;