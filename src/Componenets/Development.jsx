import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";

const Development = () => {
    const controls = useAnimation();
    const { ref, inView } = useInView();

    useEffect(() => {
        if (inView) {
            controls.start("animate");
        }
    }, [controls, inView]);


    const stagger = {
        animate: {
            transition: {
                staggerChildren: 0.3,
            },
        },
    };

    const slideIn = {
        initial: { x: -50, opacity: 0 },
        animate: { x: 0, opacity: 1, transition: { duration: 4 } },
    };


    return (
        <motion.div
            className="flex flex-col items-center justify-center"
            variants={stagger}
            initial="initial"
            animate={controls}
            ref={ref}
        >
            <motion.div variants={slideIn}>
                <div class="flex flex-wrap justify-center">
                    <div class="max-w-md w-full rounded-lg overflow-hidden shadow-lg my-4 h-auto sm:h-48">
                        <h3 className="text-gray-500 font-extrabold">PRODUCT CONCEPTUALIZATION</h3>

                        Our specialists will gather
                        all the necessary information
                        to create a comprehensive
                        understanding of your desired
                        product. Through meticulous
                        research and analysis, we
                        will develop a unique product
                        personality that aligns
                        perfectly with your
                        requirements and vision.                    </div>
                    <div class="max-w-md w-full rounded-lg overflow-hidden shadow-lg my-4">
                    <h3 className="text-gray-500  font-extrabold">PRODUCT
                            DEVELOPMENT</h3>
                        Building upon the insights
                        gained in the previous step,
                        our skilled team will embark
                        on the product creation process.
                        This involves formulating the
                        perfect blend, carefully selecting
                        components, and crafting
                        prototypes for your evaluation.
                        We strive to deliver ready-to-test
                        samples that meet your
                        expectations                       </div>
                    <div class="max-w-md w-full rounded-lg overflow-hidden shadow-lg my-4">
                    <h3 className="text-gray-500 font-extrabold">PRODUCT
                            CUSTOMIZATION</h3>  
                        Once the product creation is
                        complete, we shift our focus to
                        packaging customization. Our
                        team will guide you through an
                        array of choices, including bottle
                        designs and display cartons,
                        ensuring that they reflect your
                        brand identity, vision, and
                        budget. Together, we'll create
                        a visually striking and satisfying
                        product presentation                      </div>
                    <div class="max-w-md w-full rounded-lg overflow-hidden shadow-lg my-4">
                        <h3 className="text-gray-500 font-extrabold">MARKETING
                            PREPARATION
                        </h3> 
                        In parallel with product
                        customization, our efforts extend
                        to marketing preparation. We
                        collaborate with you to finalize
                        essential marketing elements,
                        such as brand name selection,
                        logo design,and more. Additionally,
                        our dedicated sales, marketing,
                        and advertising consultants
                        provide valuable guidance to
                        support your product's successful
                        market entry                     </div>
                </div>
            </motion.div>
        </motion.div>

    );
}

export default Development; 