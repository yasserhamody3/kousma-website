import { Link } from 'react-scroll';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { MenuIcon, XIcon } from '@heroicons/react/outline';

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(false);

    const navigate = useNavigate();
    const handleClick = () => {

        navigate('/products')
    }
    const [dropdownVisible, setDropdownVisible] = useState(false);

    function handleMouseEnter() {
        setDropdownVisible(true);
    }

    function handleMouseLeave() {
        setDropdownVisible(false);
    }


    return (
        <div>
            <div className="container   flex justify-between items-center">
                <div className="text-lg" onClick={() => {
                    navigate('/')
                }}>
                    <span className="text-blue-500 font-extrabold text-4xl">F&M</span>
                    <span className="text-gray-500 font-semibold">{"   "}Cosma</span>
                </div>
                <ul className=" text-slate-600 sm:flex  my-4 ">

                    <li className="mr-6">
                        <Link
                            to="section1"
                            spy={true}
                            smooth={true}
                            duration={500}
                            offset={-50}
                            className="text-slate-300 font-semibold hover:text-gray-400  cursor-pointer"
                            onClick={() => {
                                navigate('/')
                            }}
                        >
                            About US
                        </Link>
                    </li>
                    <li className="relative group mr-6" onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} >
                        <Link
                            spy={true}
                            smooth={true}
                            duration={500}
                            offset={-50}
                            onClick={handleClick}
                            className="text-slate-300 font-semibold hover:text-gray-400 cursor-pointer"
                        >
                            Products
                        </Link>
                    </li>
                   

                    <li className="mr-6">
                        <Link
                            to="development"
                            spy={true}
                            smooth={true}
                            duration={500}
                            offset={-50}
                            className="text-slate-300 font-semibold hover:text-gray-400 cursor-pointer"
                            onClick={() => {
                                navigate('/')
                            }}
                        >
                            Resarch & Development
                        </Link>
                    </li>
                    <li className="mr-6">
                        <Link
                            to="Quality"
                            spy={true}

                            offset={window.innerHeight / 2}
                            duration={500}
                            smooth={true}
                            className="text-slate-300 font-semibold hover:text-gray-400 cursor-pointer"
                        >
                            Quality Managment
                        </Link>
                    </li>
                    <li className="mr-6">
                        <Link
                            to="Label"
                            spy={true}

                            offset={window.innerHeight / 2}
                            duration={500}
                            smooth={true}
                            className="text-slate-300 font-semibold hover:text-gray-400 cursor-pointer"
                        >
                            Private Label
                        </Link>
                    </li>

                </ul>
            </div>

        </div >
    )
}
export default Navbar; 